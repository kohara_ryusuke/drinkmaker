<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
	    <link href="./css/top.css" rel="stylesheet" type="text/css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <title>新規投稿</title>
    </head>
    <body>
        <div class="main-contents">
			<header>
				<h1>ドリンクメーカー</h1>
			    <nav class="pc-nav">
					<a href="mypage?userId=${loginUser.id}">戻る</a>
				</nav>
			</header>
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>

            <form action="post" method="post" onsubmit="return check();"><br />
	            <div class="form">
		            <label for="drinkName">ドリンク名</label>
		            <input name="drinkName" id="drinkName" required/> <br />
		            <div id="drinkNameError" style="display:none;">ドリンク名は30文字以下で入力してください</div>

		            <label for="material">材料</label>
		            <textarea name="material" cols="100" rows="5" id="material" required></textarea>
					<div id="materialError" style="display:none;">材料は250文字以下で入力してください</div><br>

					<label for="taste">味</label>
	                <div class="select">
						<select name="taste">
							<c:forEach items="${tastes}" var="taste">
								<option value="${taste}">${taste}</option>
							</c:forEach>
						</select>
					</div>

					<label for="color">色</label>
		            <div class="select">
						<select name="color">
							<c:forEach items="${colors}" var="color">
								<option value="${color}">${color}</option>
							</c:forEach>
						</select>
					</div>

		            <label for="process">作り方</label>
		            <textarea name="process" cols="100" rows="5" id="process" required></textarea>
					<div id="processError" style="display:none;">作り方は250文字以下で入力してください</div><br>

		            <label for="point">おすすめポイント</label>
		            <textarea name="point" cols="100" rows="5" id="point" required></textarea>
					<div id="pointError" style="display:none;">おすすめポイントは250文字以下で入力してください</div><br>

					<input type="hidden" name="userId" id="userId" value="${loginUser.id}" /> <br />
		            <input type="submit" value="新規投稿" class="button"/> <br />
	            </div>
	        </form>
            <div class="copyright">Copyright(c)Ryusuke Kohara</div>
        </div>

        <script type="text/javascript">
        function check(){
        	const drinkName = document.getElementById('drinkName').value;
        	const material = document.getElementById('material').value;
        	const process = document.getElementById('process').value;
        	const point = document.getElementById('point').value;
        	let count = 0;
        	if(drinkName.length > 30){
        		document.getElementById('drinkNameError').style.display = "block";
        		count = 1;
        	}
        	if(material.length > 250){
        		document.getElementById('materialError').style.display = "block";
        		count = 1;
        	}
        	if(process.length > 250){
        		document.getElementById('processError').style.display = "block";
        		count = 1;
        	}
        	if(point.length > 250){
        		document.getElementById('pointError').style.display = "block";
        		count = 1;
        	}
        	if(count == 1){
        		return false;
        	}
        }
        </script>

    </body>
</html>