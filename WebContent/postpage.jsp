<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    	<link href="./css/postpage.css" rel="stylesheet" type="text/css">
    	<script src="https://code.jquery-3.5.1.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ドリンクメーカー</title>
    </head>
    <body>
        <div class="main-contents">
			<header>
			    <h1>ドリンクメーカー</h1>
			    <nav class="pc-nav">
					<a href="./">戻る</a>
				</nav>
			</header>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<div class="post">
			<div class="flex-item">
				<div class="parts">
					<div class="title">ドリンク名</div>
					<p style="white-space: pre-wrap;">${post.drinkName}</p>
				</div>
				<div class="parts">
					<div class="title">材料</div>
					<p style="white-space: pre-wrap;">${post.material}</p>
				</div>
				<div class="parts">
					<div class="title">味</div>
					<p style="white-space: pre-wrap;">${post.taste}</p>
				</div>
				<div class="parts">
					<div class="title">色</div>
					<p style="white-space: pre-wrap;">${post.color}</p>
				</div>
				<div class="parts">
					<div class="title">投稿日時</div>
				<fmt:formatDate value="${post.updatedDate}"
					pattern="yyyy/MM/dd HH:mm:ss" />
				</div>
			</div>
			<div class="flex-item">

				<div class="parts">
					<div class="title">作り方</div>
					<p style="white-space: pre-wrap;">${post.process}</p>
				</div>
				<div class="parts">
					<div class="title">おすすめポイント</div>
					<p style="white-space: pre-wrap;">${post.point}</p>
				</div>
			</div>
		</div>

		<div class="copyright">Copyright(c)Ryusuke Kohara</div>
		</div>
    </body>
</html>