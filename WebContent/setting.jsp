<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    	<link href="./css/top.css" rel="stylesheet" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>設定</title>
    </head>
    <body>
        <div class="main-contents">
        	<header>
        		<h1>ドリンクメーカー</h1>
			    <nav class="pc-nav">
					<a href="mypage?userId=${loginUser.id}">戻る</a>
				</nav>
			</header>
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>

	        <form action="setting" method="post" onsubmit="return check();"><br />
	            <div class="form">
	                <label for="name">ユーザー名</label>
	                <input name="name" id="name" value="${loginUser.name}" placeholder="20文字以下" required/> <br />
	                <div id="nameError" style="display:none;">ユーザー名は20文字以下で入力してください(スペース不可)</div><br>

	                <label for="oldPass">現在のパスワード</label>
	                <input name="oldPass" type="password" id="oldPass" placeholder="半角英数20文字以下" required/> <br />
					<div id="oldPassError" style="display:none;">現在のパスワードは半角英数20文字以下で入力してください</div><br>

	                <label for="newPass">新しいパスワード</label>
	                <input name="newPass" type="password" id="newPass" placeholder="半角英数20文字以下" /> <br />
	                <div id="newPassError" style="display:none;">新しいパスワードは半角英数20文字以下で入力してください</div><br>

	                <label for="conNewPass">新しいパスワード(確認用)</label>
	                <input name="conNewPass" type="password" id="conNewPass"/> <br />
	                <div id="conNewPassError" style="display:none;">新しいパスワード(確認用)は半角英数20文字以下で入力してください</div><br>

					<input type="hidden" name="userId" id="userId" value="${loginUser.id}" />
	                <input type="submit" value="変更" class="button"/>
	            </div>
	        </form>
            <div class="copyright"> Copyright(c)Ryusuke Kohara</div>
        </div>

        <script type="text/javascript">
        function check(){
        	const name = document.getElementById('name').value;
        	const oldPass = document.getElementById('oldPass').value;
        	const newPass = document.getElementById('newPass').value;
        	const conNewPass = document.getElementById('conNewPass').value;
        	let count = 0;
        	if(/\s/.test(name) || name.length > 20){
        		document.getElementById('nameError').style.display = "block";
        		count = 1;
        	}
        	if(!oldPass.match(/^[A-Za-z0-9]+$/) || oldPass.length > 20){
        		document.getElementById('oldPassError').style.display = "block";
        		count = 1;
        	} else if(!(!newPass || !conNewPass)){
        		if (!newPass && (!newPass.match(/^[A-Za-z0-9]+$/) || newPass.length > 20)){
	        		document.getElementById('newPassError').style.display = "block";
	        		count = 1;
        		}
	        	if(!conNewPass && (!conNewPass.match(/^[A-Za-z0-9]+$/) || conNewPass.length > 20)){
	        		document.getElementById('conNewPassError').style.display = "block";
	        		count = 1;
	        	}
        	}
        	if(count == 1){
        		return false;
        	}
        }
        </script>

    </body>
</html>
