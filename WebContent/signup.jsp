<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
	    <link href="./css/top.css" rel="stylesheet" type="text/css">
	    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <title>ユーザー新規登録</title>
    </head>
    <body>
        <div class="main-contents">
			<header>
				<h1>ドリンクメーカー</h1>
			    <nav class="pc-nav">
					<a href="./">戻る</a>
				</nav>
			</header>
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>

            <form action="signup" method="post" onsubmit="return check();"><br />
	            <div class="form">
	                <label for="name">ユーザー名</label>
	                <input name="name" id="name" placeholder="20文字以下" required/> <br />
					<div id="nameError" style="display:none;">ユーザー名は20文字以下で入力してください</div><br>

	                <label for="password">パスワード</label>
	                <input name="password" type="password" id="password" placeholder="半角英数20文字以下" required/> <br />
	                <div id="passwordError" style="display:none;">パスワードは半角英数20文字以下で入力してください</div><br>

	                <input type="submit" value="登録" class="button"/> <br />
	            </div>
	        </form>
            <div class="copyright">Copyright(c)Ryusuke Kohara</div>
        </div>

        <script type="text/javascript">
        function check(){
        	const name = document.getElementById('name').value;
        	const password = document.getElementById('password').value;
        	let count = 0;
        	if(/\s/.test(name) || name.length > 20){
        		document.getElementById('nameError').style.display = "block";
        		count = 1;
        	}
        	if(!password.match(/^[A-Za-z0-9]+$/) || password.length > 20){
        		document.getElementById('passwordError').style.display = "block";
        		count = 1;
        	}
        	if(count == 1){
        		return false;
        	}
        }
        </script>
    </body>
</html>