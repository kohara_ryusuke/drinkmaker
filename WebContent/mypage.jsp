<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    	<link href="./css/top.css" rel="stylesheet" type="text/css">
    	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
      	<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    	<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>マイページ</title>
    </head>
    <body>
        <div class="main-contents">
        	<header>
				<h1>ドリンクメーカー</h1>
				<nav class="pc-nav">
			    <ul>
			    	<li><a href="./">戻る</a></li>
					<li><a href="setting">設定</a></li>
					<li><a href="post">新規投稿</a></li>
				</ul>
				</nav>
			</header>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<div class="posts">
			<c:forEach items="${myposts}" var="post">
				<div class="post">
					<div class="drinkName">
						<a href="postpage?postId=${post.id}">${post.drinkName}</a>
						<a href="postedit?postId=${post.id}">編集</a>
						<a href="postdelete?postId=${post.id}&userId=${loginUser.id}" onclick="return confirm('削除しますか？')">削除</a>
					</div>
					<div class="taste">
						<p>${post.taste}</p>
					</div>
					<div class="color">
						<p>${post.color}</p>
					</div>
					<div class="date">
						<fmt:formatDate value="${post.createdDate}"
							pattern="yyyy/MM/dd HH:mm:ss" />
					</div>
				</div>
			</c:forEach>
		</div>

		<div class="posts">
			<c:forEach items="${likeposts}" var="post">
				<div class="post">
					<div class="drinkName">
						<a href="postpage?postId=${post.id}">${post.drinkName}</a>
						<c:if test="${post.userId == loginUser.id}">
							<a href="postedit?postId=${post.id}">編集</a>
							<a href="postdelete?postId=${post.id}&userId=${loginUser.id}" onclick="return confirm('削除しますか？')">削除</a>
						</c:if>
					</div>
					<div class="taste">
						<p>${post.taste}</p>
					</div>
					<div class="color">
						<p>${post.color}</p>
					</div>
					<div class="date">
						<fmt:formatDate value="${post.createdDate}"
							pattern="yyyy/MM/dd HH:mm:ss" />
					</div>
					<span class="fa fa-heart like-btn-like" id="${post.id}"></span>
				</div>
			</c:forEach>
			<input type="hidden" id="userId" value="${loginUser.id}" />
		</div>
		<div class="copyright">Copyright(c)Ryusuke Kohara</div>
		</div>

		<script type="text/javascript">
		$(function(){
			$('.fa-heart').on('click',function(){
				var postId = $(this).attr("id");
				var s = document.getElementById(postId).className;
				if(s == "fa fa-heart like-btn-unlike"){
					$.ajax({
					    url: "like",
				        type: "POST",
				        data: {
				        	likeFlg : "true",
				        	postId : postId,
				        	userId : $('#userId').val()
				        	}
				    }).done(function (result) {
				    	document.getElementById(result).style.color = "#ff2581";
				    	document.getElementById(result).className = "fa fa-heart like-btn-like";
			        }).fail(function () {
			          alert("読み込み失敗");
			        }).always(function (result) {
			          // 常に実行する処理
				    });
				} else if(s == "fa fa-heart like-btn-like"){
					    $.ajax({
					      url: "like",
				          type: "POST",
				          data: {
				        	  likeFlg : "false",
				        	  postId : postId,
				        	  userId : $('#userId').val()
				        	  }
				        }).done(function (result) {
				        	document.getElementById(result).style.color = "#8899a6";
				        	document.getElementById(result).className = "fa fa-heart like-btn-unlike";
				        }).fail(function () {
				          alert("読み込み失敗");
				        }).always(function (result) {
				          // 常に実行する処理
					    });
				}
			});
		});
        </script>

    </body>
</html>