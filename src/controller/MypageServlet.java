package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Like;
import beans.Post;
import beans.User;
import service.LikeService;
import service.PostService;

@WebServlet(urlPatterns = { "/mypage" })
public class MypageServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        HttpSession session = request.getSession();
        User loginUser = (User)session.getAttribute("loginUser");
    	int loginUserId = loginUser.getId();
    	List<Post> myposts = new PostService().mypageSelect(loginUserId);

    	List<Like> likes = new LikeService().select(loginUser.getId());
    	List<Post> allPosts = new PostService().select();
    	List<Post> likeposts = new ArrayList<>();
    	for(int i = 0 ;allPosts.size() > i ;i++) {
    		for(int j = 0 ;likes.size() > j ;j++) {
    			if(allPosts.get(i).getId() == likes.get(j).getPostId()) {
    				likeposts.add(allPosts.get(i));
    			}
    		}
    	}

    	request.setAttribute("myposts", myposts);
    	request.setAttribute("likeposts", likeposts);
    	request.getRequestDispatcher("mypage.jsp").forward(request, response);
    }
}
