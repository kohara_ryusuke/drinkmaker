package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Post;
import beans.User;
import service.PostService;

@WebServlet(urlPatterns = { "/post" })
public class PostServlet extends HttpServlet {

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

		List<String> tastes = new ArrayList<>
		(Arrays.asList("甘い","ほんのり甘い", "すっきりキリッと","ほんのり苦い","飲みやすい","パンチある","その他"));
		List<String> colors = new ArrayList<>
		(Arrays.asList("透明","赤系", "青系","緑系","黄色系","白系","その他"));
		request.setAttribute("tastes", tastes);
		request.setAttribute("colors", colors);
        request.getRequestDispatcher("newpost.jsp").forward(request, response);
    }

	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();

        Post post = getPost(request);
        if (!isValid(post, errorMessages)) {
            session.setAttribute("errorMessages", errorMessages);
            response.sendRedirect("./post");
            return;
        }

        User user = (User) session.getAttribute("loginUser");
        post.setUserId(user.getId());

        new PostService().insert(post);
        response.sendRedirect("./");
    }

    private Post getPost(HttpServletRequest request) throws IOException, ServletException {
        Post post = new Post();
        post.setUserId(Integer.parseInt(request.getParameter("userId")));
        post.setDrinkName(request.getParameter("drinkName"));
        post.setMaterial(request.getParameter("material"));
        post.setTaste(request.getParameter("taste"));
        post.setColor(request.getParameter("color"));
        post.setProcess(request.getParameter("process"));
        post.setPoint(request.getParameter("point"));
        return post;
    }

    private boolean isValid(Post post, List<String> errorMessages) {

        String drinkName = post.getDrinkName();
        String material = post.getMaterial();
        String process = post.getProcess();
        String point = post.getPoint();

    	if (StringUtils.isEmpty(drinkName)) {
            errorMessages.add("ドリンク名を入力してください");
        } else if (30 < drinkName.length()) {
            errorMessages.add("ドリンク名は30文字以下で入力してください！");
        }
    	if (StringUtils.isEmpty(material)) {
            errorMessages.add("材料名を入力してください");
        } else if (250 < material.length()) {
            errorMessages.add("材料名は250文字以下で入力してください！");
        }
    	if (StringUtils.isEmpty(process)) {
            errorMessages.add("作り方を入力してください");
        } else if (250 < process.length()) {
            errorMessages.add("作り方は250文字以下で入力してください！");
        }
    	if (StringUtils.isEmpty(point)) {
            errorMessages.add("おすすめポイントを入力してください");
        } else if (250 < point.length()) {
            errorMessages.add("おすすめポイントは250文字以下で入力してください！");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}
