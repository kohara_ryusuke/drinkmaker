package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Post;
import exception.NoRowsUpdatedRuntimeException;
import service.PostService;

@WebServlet(urlPatterns = { "/postedit" })
public class PostEditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	int postId = Integer.parseInt(request.getParameter("postId"));
    	Post post = new PostService().select(postId);
    	List<String> tastes = new ArrayList<>
		(Arrays.asList("甘い","ほんのり甘い", "すっきりキリッと","ほんのり苦い","飲みやすい","パンチある","その他"));
		List<String> colors = new ArrayList<>
		(Arrays.asList("透明","赤系", "青系","緑系","黄色系","白系","その他"));

		request.setAttribute("tastes", tastes);
		request.setAttribute("colors", colors);
    	request.setAttribute("post", post);
        request.getRequestDispatcher("postedit.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();

        Post post = getPost(request);
        if (isValid(post, errorMessages)) {
            try {
                new PostService().update(post);
            } catch (NoRowsUpdatedRuntimeException e) {
                errorMessages.add("予期せぬエラーが発生しました");
            }
        } else if (errorMessages.size() != 0) {
            session.setAttribute("errorMessages", errorMessages);
            response.sendRedirect("./postedit?postId=" + post.getId());
            return;
        }
        response.sendRedirect("./mypage?userId=" + post.getUserId());
    }

    private Post getPost(HttpServletRequest request) throws IOException, ServletException {
        Post post = new Post();
        post.setId(Integer.parseInt(request.getParameter("postId")));
        post.setDrinkName(request.getParameter("drinkName"));
        post.setMaterial(request.getParameter("material"));
        post.setTaste(request.getParameter("taste"));
        post.setColor(request.getParameter("color"));
        post.setProcess(request.getParameter("process"));
        post.setPoint(request.getParameter("point"));
        return post;
    }

    private boolean isValid(Post post, List<String> errorMessages) {

        String drinkName = post.getDrinkName();
        String material = post.getMaterial();
        String process = post.getProcess();
        String point = post.getPoint();

    	if (StringUtils.isEmpty(drinkName)) {
            errorMessages.add("ドリンク名を入力してください");
        } else if (30 < drinkName.length()) {
            errorMessages.add("ドリンク名は30文字以下で入力してください！");
        }
    	if (StringUtils.isEmpty(material)) {
            errorMessages.add("材料名を入力してください");
        } else if (250 < material.length()) {
            errorMessages.add("材料名は250文字以下で入力してください！");
        }
    	if (StringUtils.isEmpty(process)) {
            errorMessages.add("作り方を入力してください");
        } else if (250 < process.length()) {
            errorMessages.add("作り方は250文字以下で入力してください！");
        }
    	if (StringUtils.isEmpty(point)) {
            errorMessages.add("おすすめポイントを入力してください");
        } else if (250 < point.length()) {
            errorMessages.add("おすすめポイントは250文字以下で入力してください！");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}

