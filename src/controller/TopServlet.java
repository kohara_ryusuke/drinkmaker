package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Like;
import beans.Post;
import beans.User;
import service.LikeService;
import service.PostService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	User user = (User) request.getSession().getAttribute("loginUser");
        if (user != null) {
        	List<Like> likes = new LikeService().select(user.getId());
        	request.setAttribute("likes", likes);
        }

        List<Post> posts = new PostService().select();

        request.setAttribute("posts", posts);
        request.getRequestDispatcher("/top.jsp").forward(request, response);
    }
}
