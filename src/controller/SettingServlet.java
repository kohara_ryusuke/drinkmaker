package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        request.getRequestDispatcher("setting.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();

        User user = getUser(request);
        if (isValid(user, errorMessages, request)) {
            new UserService().update(user);
        } else {
            request.setAttribute("errorMessages", errorMessages);
            request.setAttribute("user", user);
            request.getRequestDispatcher("setting.jsp").forward(request, response);
            return;
        }

        session.setAttribute("loginUser", user);
        response.sendRedirect("./");
    }

    private User getUser(HttpServletRequest request) throws IOException, ServletException {

        User user = new User();
        user.setId(Integer.parseInt(request.getParameter("userId")));
        user.setName(request.getParameter("name"));
        return user;
    }

    private boolean isValid(User user, List<String> errorMessages, HttpServletRequest request) {

        String name = user.getName();
        String oldPass = request.getParameter("oldPass");
        String newPass = request.getParameter("newPass");
        String conNewPass = request.getParameter("conNewPass");

        if (StringUtils.isEmpty(name)) {
            errorMessages.add("ユーザー名を入力してください");
        } else if (20 < name.length()) {
            errorMessages.add("ユーザー名は20文字以下で入力してください");
        }

        if (StringUtils.isEmpty(oldPass)) {
            errorMessages.add("現在のパスワードを入力してください");
        } else if (!new UserService().passMatch(user.getId(), oldPass)) {
        	errorMessages.add("現在のパスワードが間違っています");
        } else if(!StringUtils.isEmpty(newPass) || !StringUtils.isEmpty(conNewPass)) {
        	if (!StringUtils.isEmpty(newPass) && (!Pattern.matches("^[A-Za-z0-9]+$", newPass) || 20 < newPass.length())) {
            	errorMessages.add("新しいパスワードは半角英数20文字以下で入力してください");
            } else {
                if(newPass.equals(conNewPass)) {
                   	user.setPassword(newPass);
                } else {
                   	errorMessages.add("新しいパスワードと新しいパスワード(確認用)が異なっています");
                }
            }
        }else {
        	user.setPassword(oldPass);
    	}

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}

