package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Post;
import service.PostService;

@WebServlet(urlPatterns = { "/postpage" })
public class PostpageServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	int postId = Integer.parseInt(request.getParameter("postId"));
    	Post post = new PostService().select(postId);

    	request.setAttribute("post", post);
        request.getRequestDispatcher("postpage.jsp").forward(request, response);

    }
}
