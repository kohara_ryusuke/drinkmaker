package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.PostService;

@WebServlet(urlPatterns = { "/postdelete" })
public class PostDeleteServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		int userId = Integer.parseInt(request.getParameter("userId"));
		int postId = Integer.parseInt(request.getParameter("postId"));
		new PostService().delete(postId);
		response.sendRedirect("./mypage?userId=" + userId);
	}
}


