package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Post;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class PostDao {

  public void insert(Connection connection, Post post) {

    PreparedStatement ps = null;
    try {
      StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO posts ( ");
            sql.append("    user_id, ");
            sql.append("    drink_name, ");
            sql.append("    material, ");
            sql.append("    taste, ");
            sql.append("    color, ");
            sql.append("    process, ");
            sql.append("    point, ");
            sql.append("    created_date, ");
            sql.append("    updated_date ");
            sql.append(") VALUES ( ");
            sql.append("    ?, ");                                  // user_id
            sql.append("    ?, ");                                  // drink_name
            sql.append("    ?, ");                                  // material
            sql.append("    ?, ");                                  // taste
            sql.append("    ?, ");                                  // color
            sql.append("    ?, ");                                  // process
            sql.append("    ?, ");                                  // point
            sql.append("    CURRENT_TIMESTAMP, ");  // created_date
            sql.append("    CURRENT_TIMESTAMP ");       // updated_date
            sql.append(")");

      ps = connection.prepareStatement(sql.toString());
      ps.setInt(1, post.getUserId());
      ps.setString(2, post.getDrinkName());
      ps.setString(3, post.getMaterial());
      ps.setString(4, post.getTaste());
      ps.setString(5, post.getColor());
      ps.setString(6, post.getProcess());
      ps.setString(7, post.getPoint());

      ps.executeUpdate();
    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
  }

  public List<Post> select(Connection connection) {
      PreparedStatement ps = null;
      try {
          String sql = "SELECT * FROM posts ORDER BY created_date DESC";

          ps = connection.prepareStatement(sql);
          ResultSet rs = ps.executeQuery();

          List<Post> posts = toPosts(rs);
          return posts;
      } catch (SQLException e) {
          throw new SQLRuntimeException(e);
      } finally {
          close(ps);
      }
  }

  public Post select(Connection connection, int postId) {
      PreparedStatement ps = null;
      try {
          String sql = "SELECT * FROM posts WHERE id = ? ";

          ps = connection.prepareStatement(sql);
          ps.setInt(1, postId);
          ResultSet rs = ps.executeQuery();

          List<Post> posts = toPosts(rs);
          if (posts.isEmpty()) {
              return null;
          } else if (2 <= posts.size()) {
              throw new IllegalStateException("予期せぬエラーが発生しました");
          } else {
              return posts.get(0);
          }
      } catch (SQLException e) {
          throw new SQLRuntimeException(e);
      } finally {
          close(ps);
      }
  }

  public List<Post> mypageSelect(Connection connection, int userId) {
      PreparedStatement ps = null;
      try {
          String sql = "SELECT * FROM posts WHERE user_id = ? ORDER BY created_date DESC";

          ps = connection.prepareStatement(sql);
          ps.setInt(1, userId);
          ResultSet rs = ps.executeQuery();

          List<Post> posts = toPosts(rs);
          return posts;
      } catch (SQLException e) {
          throw new SQLRuntimeException(e);
      } finally {
          close(ps);
      }
  }

  public void update(Connection connection, Post post) {
      PreparedStatement ps = null;
      try {
          StringBuilder sql = new StringBuilder();
          sql.append("UPDATE posts SET ");
          sql.append("    drink_name = ?, ");
          sql.append("    material = ?, ");
          sql.append("    taste = ?, ");
          sql.append("    color = ?, ");
          sql.append("    process = ?, ");
          sql.append("    point = ?, ");
          sql.append("    updated_date = CURRENT_TIMESTAMP ");
          sql.append("    WHERE id = ?");

          ps = connection.prepareStatement(sql.toString());
          ps.setString(1, post.getDrinkName());
          ps.setString(2, post.getMaterial());
          ps.setString(3, post.getTaste());
          ps.setString(4, post.getColor());
          ps.setString(5, post.getProcess());
          ps.setString(6, post.getPoint());
          ps.setInt(7, post.getId());

          int count = ps.executeUpdate();
          if (count == 0) {
              throw new NoRowsUpdatedRuntimeException();
          }
      } catch (SQLException e) {
          throw new SQLRuntimeException(e);
      } finally {
          close(ps);
      }
  }

  private List<Post> toPosts(ResultSet rs) throws SQLException {
      List<Post> posts = new ArrayList<Post>();
      try {
          while (rs.next()) {
              Post post = new Post();
              post.setId(rs.getInt("id"));
              post.setUserId(rs.getInt("user_id"));
              post.setDrinkName(rs.getString("drink_name"));
              post.setMaterial(rs.getString("material"));
              post.setTaste(rs.getString("taste"));
              post.setColor(rs.getString("color"));
              post.setProcess(rs.getString("process"));
              post.setPoint(rs.getString("point"));
              post.setCreatedDate(rs.getTimestamp("created_date"));
              post.setUpdatedDate(rs.getTimestamp("updated_date"));
              posts.add(post);
          }
          return posts;
      } finally {
          close(rs);
      }
  }

  public void delete(Connection connection, int postId) {
      PreparedStatement ps = null;
      try {
          String sql = "DELETE FROM posts WHERE id = ?";

          ps = connection.prepareStatement(sql);
          ps.setInt(1, postId);
          int count = ps.executeUpdate();
          if (count == 0) {
              throw new NoRowsUpdatedRuntimeException();
          }

      } catch (SQLException e) {
          throw new SQLRuntimeException(e);
      } finally {
          close(ps);
      }
  }
}
