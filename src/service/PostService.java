package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Post;
import dao.PostDao;

public class PostService {

    public void insert(Post post) {

        Connection connection = null;
        try {
            connection = getConnection();
            new PostDao().insert(connection, post);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<Post> select() {
        Connection connection = null;
        try {
            connection = getConnection();
            List<Post> post = new PostDao().select(connection);
            commit(connection);
            return post;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public Post select(int postId) {
        Connection connection = null;
        try {
            connection = getConnection();
            Post post = new PostDao().select(connection, postId);
            commit(connection);
            return post;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<Post> mypageSelect(int userId) {
        Connection connection = null;
        try {
            connection = getConnection();
            List<Post> post = new PostDao().mypageSelect(connection, userId);
            commit(connection);
            return post;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void update(Post post) {
        Connection connection = null;
        try {
            connection = getConnection();
            new PostDao().update(connection, post);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void delete(int postId) {

        Connection connection = null;
        try {
            connection = getConnection();
            new PostDao().delete(connection, postId);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}